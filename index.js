const http = require('http');
const port = process.env.PORT || 3000;

const server = http.createServer((req, res) => {
  let time1 = new Date().getTime();
  let b =0;
  for(let i=0;i<100000;i++){
    b++;
  }
  res.statusCode = 200;

  let time2 = new Date().getTime()
  let diff = time2-time1;
  res.end('Hello Node!\n'+diff);
});

server.listen(port, () => {
  console.log(`Server running on http://localhost:${port}/`);
});
